terraform {
  cloud {
    organization = "tfl_lab"

    workspaces {
      name = "my-example"
    }
  }
}
